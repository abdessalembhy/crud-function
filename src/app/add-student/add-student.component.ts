import { Component, OnInit, ViewChild } from '@angular/core';
import { Student } from '../shared/models/student';
import { StudentService } from '../shared/services/student.service';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-student',
  templateUrl: './add-student.component.html',
  styleUrls: ['./add-student.component.css']
})
export class AddStudentComponent implements OnInit {
  public student = new Student();
  @ViewChild('addForm', {static: false}) addForm: NgForm;
  constructor(private studentService: StudentService, private route: ActivatedRoute,
              private toastr: ToastrService) { }

  ngOnInit(): void {
    const id = this.route.snapshot.queryParams['idStudent'];
    if (id) {
      this.getStudent(id);
    }
  }
  save() {
    if (this.addForm.valid) {
      if (!this.student.id) {
        this.studentService.AddStudent({ ...this.student }).then((res) => {
          // this.formAdd.resetForm();
          this.toastr.success('Hello world!', 'Toastr fun!');
        });
      } else {
        this.studentService.updateStudent(this.student);
      }
    }
    this.addForm.resetForm();
  }
    getStudent(id) {
      this.studentService.getStudent(id).subscribe(res => {
      this.student = res.data() as Student;
      this.student.id = res.id;
      });
      }
}
