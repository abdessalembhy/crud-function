export class Student {
    lastName: string;
    firstName: string;
    age: number;
    email: string;
    password: string;
    phone:number;
    id:string;
}
