// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyA2Dohy99ERe1EFjAD4csQXFjtfjluxfIs",
    authDomain: "crud-firebase-deb5f.firebaseapp.com",
    databaseURL: "https://crud-firebase-deb5f.firebaseio.com",
    projectId: "crud-firebase-deb5f",
    storageBucket: "crud-firebase-deb5f.appspot.com",
    messagingSenderId: "937215915215",
    appId: "1:937215915215:web:011f48940bb805931d33ba",
    measurementId: "G-N0XK26B1DQ"
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
